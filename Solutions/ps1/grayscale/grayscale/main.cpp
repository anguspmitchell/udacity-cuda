//Udacity HW1 Solution

#include <iostream>
#include "timer.h"
#include "utils.h"
#include <string>
#include <stdio.h>
#include "reference_calc.h"
#include "compare.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

void your_rgba_to_greyscale(const uchar4 * const h_rgbaImage, 
                            uchar4 * const d_rgbaImage,
                            unsigned char* const d_greyImage, 
                            size_t numRows, size_t numCols, int blockSize_i, int blockSize_j);

//include the definitions of the above functions for this homework
#include "HW1.cpp"

int main(int argc, char **argv) {
  uchar4        *h_rgbaImage, *d_rgbaImage;
  unsigned char *h_greyImage, *d_greyImage;

  std::string input_file;
  std::string output_file;
  std::string reference_file;
  double perPixelError = 0.0;
  double globalError   = 0.0;
  bool useEpsCheck = false;
  switch (argc)
  {
	case 2:
	  input_file = std::string(argv[1]);
	  output_file = "HW1_output.png";
	  reference_file = "HW1_reference.png";
	  break;
	case 3:
	  input_file  = std::string(argv[1]);
      output_file = std::string(argv[2]);
	  reference_file = "HW1_reference.png";
	  break;
	case 4:
	  input_file  = std::string(argv[1]);
      output_file = std::string(argv[2]);
	  reference_file = std::string(argv[3]);
	  break;
	case 6:
	  useEpsCheck=true;
	  input_file  = std::string(argv[1]);
	  output_file = std::string(argv[2]);
	  reference_file = std::string(argv[3]);
	  perPixelError = atof(argv[4]);
      globalError   = atof(argv[5]);
	  break;
	default:
      std::cerr << "Usage: ./HW1 input_file [output_filename] [reference_filename] [perPixelError] [globalError]" << std::endl;
      exit(1);
  }  
  
  int blockSize_i;
  int blockSize_j;
  for (int j = 0; j < 2; j++){
	  for (int i = 1; i <= 32; i++){
		  if (j == 0){
			  blockSize_i = i;
			  blockSize_j = i;
		  }
		  else{
			  blockSize_i = i * i;
			  blockSize_j = 1;
		  }
		  if (i == 33){
			  blockSize_i = 32;
			  blockSize_j = 16;
		  }
		  int gridSize_i = (numRows() + blockSize_i - 1) / blockSize_i;
		  int gridSize_j = (numCols() + blockSize_j - 1) / blockSize_j;
		  //load the image and give us our input and output pointers
		  preProcess(&h_rgbaImage, &h_greyImage, &d_rgbaImage, &d_greyImage, input_file);

		  GpuTimer timer;
		  timer.Start();
		  //call the students' code

		  your_rgba_to_greyscale(h_rgbaImage, d_rgbaImage, d_greyImage, numRows(), numCols(), blockSize_i, blockSize_j);
		  timer.Stop();
		  cudaDeviceSynchronize(); // checkCudaErrors(cudaGetLastError());

		  int err = printf("G: %d x %d (%d) B: %d x %d (%d) Your code ran in: %f msecs.", blockSize_i, blockSize_j, blockSize_i * blockSize_j, gridSize_i, gridSize_j, gridSize_i * gridSize_j, timer.Elapsed());

		  if (err < 0) {
			  //Couldn't print! Probably the student closed stdout - bad news
			  std::cerr << "Couldn't print timing information! STDOUT Closed!" << std::endl;
			  exit(1);
		  }

		  size_t numPixels = numRows()*numCols();
		  checkCudaErrors(cudaMemcpy(h_greyImage, d_greyImage, sizeof(unsigned char) * numPixels, cudaMemcpyDeviceToHost));

		  //check results and output the grey image
		  postProcess(output_file, h_greyImage);

		  referenceCalculation(h_rgbaImage, h_greyImage, numRows(), numCols());

		  postProcess(reference_file, h_greyImage);

		  compareImages(reference_file, output_file, useEpsCheck, perPixelError,
			  globalError);
	  }
  }
  // Show final pictures
  cv::Mat referenceImage;
  cv::Mat outputImage;
  cv::Mat inputImage;

  printf("%s %s %s", referenceImage, outputImage, inputImage);
  referenceImage = cv::imread(reference_file, CV_LOAD_IMAGE_COLOR);
  outputImage = cv::imread(output_file, CV_LOAD_IMAGE_COLOR);
  inputImage = cv::imread(input_file, CV_LOAD_IMAGE_COLOR);

  cv::namedWindow("Reference", CV_WINDOW_AUTOSIZE);
  cv::imshow("Reference", referenceImage);
  cv::namedWindow("Output", CV_WINDOW_AUTOSIZE);
  cv::imshow("Output", outputImage);
  cv::namedWindow("Input", CV_WINDOW_AUTOSIZE);
  cv::imshow("Input", inputImage);
  cvWaitKey(0);

  //generateReferenceImage(input_file, reference_file);



  cleanup();

  return 0;
}
