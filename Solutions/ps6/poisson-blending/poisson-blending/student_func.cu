//Udacity HW 6
//Poisson Blending

/* Background
   ==========

   The goal for this assignment is to take one image (the source) and
   paste it into another image (the destination) attempting to match the
   two images so that the pasting is non-obvious. This is
   known as a "seamless clone".

   The basic ideas are as follows:

   1) Figure out the interior and border of the source image
   2) Use the values of the border pixels in the destination image 
      as boundary conditions for solving a Poisson equation that tells
      us how to blend the images.
   
      No pixels from the destination except pixels on the border
      are used to compute the match.

   Solving the Poisson Equation
   ============================

   There are multiple ways to solve this equation - we choose an iterative
   method - specifically the Jacobi method. Iterative methods start with
   a guess of the solution and then iterate to try and improve the guess
   until it stops changing.  If the problem was well-suited for the method
   then it will stop and where it stops will be the solution.

   The Jacobi method is the simplest iterative method and converges slowly - 
   that is we need a lot of iterations to get to the answer, but it is the
   easiest method to write.

   Jacobi Iterations
   =================

   Our initial guess is going to be the source image itself.  This is a pretty
   good guess for what the blended image will look like and it means that
   we won't have to do as many iterations compared to if we had started far
   from the final solution.

   ImageGuess_prev (Floating point)
   ImageGuess_next (Floating point)

   DestinationImg
   SourceImg

   Follow these steps to implement one iteration:

   1) For every pixel p in the interior, compute two sums over the four neighboring pixels:
      Sum1: If the neighbor is in the interior then += ImageGuess_prev[neighbor]
             else if the neighbor in on the border then += DestinationImg[neighbor]

      Sum2: += SourceImg[p] - SourceImg[neighbor]   (for all four neighbors)

   2) Calculate the new pixel value:
      float newVal= (Sum1 + Sum2) / 4.f  <------ Notice that the result is FLOATING POINT
      ImageGuess_next[p] = min(255, max(0, newVal)); //clamp to [0, 255]


    In this assignment we will do 800 iterations.
   */


#include "utils.h"
#include <thrust/host_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/extrema.h>

const int MAX_CUDA_THREADS = 1024;

// Taken from stackoverflow
int roundUpPower2(int n)
{
	n--;           // 1000 0011 --> 1000 0010
	n |= n >> 1;   // 1000 0010 | 0100 0001 = 1100 0011
	n |= n >> 2;   // 1100 0011 | 0011 0000 = 1111 0011
	n |= n >> 4;   // 1111 0011 | 0000 1111 = 1111 1111
	n |= n >> 8;   // ... (At this point all bits are 1, so further bitwise-or
	n |= n >> 16;  //      operations produce no effect.)
	n++;           // 1111 1111 --> 1 0000 0000
	return n;
}

/***********************************
      Mask/Interior/Exterior
***********************************/
__global__ void calculateMask(const uchar4* const img, int* const mask, int* const maskBorder, int* const maskInterior,
	const size_t numRows, const size_t numCols)
{
	/* Uchar4
		x: red
		y: green
		z: blue
		w: alpha
	*/
	
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	int j = threadIdx.y + blockDim.y * blockIdx.y;
	if (i >= numRows || j >= numCols){ return; }

	int idx_img = j + i * numCols;
	int idx_block = threadIdx.y + threadIdx.x * blockDim.y; // j + i * numCols
	
	extern __shared__ uchar4 sh_img[];
	sh_img[idx_block] = img[idx_img]; 
	__syncthreads();

	uchar4 pixel, pixelTop, pixelBottom, pixelLeft, pixelRight;
	bool inMask = true, top = true, bottom = true, left = true, right = true;

	pixel = sh_img[idx_block];
	inMask = pixel.x != 255 || pixel.y != 255 || pixel.z != 255;

	if (i - 1 >= 0){ // top
		if (threadIdx.x != 0){ // local top
			pixelTop = sh_img[threadIdx.y + (threadIdx.x - 1) * blockDim.y];
		}
		else{
			pixelTop = img[j + (i - 1) * numCols];
		}
		top = pixelTop.x != 255 || pixelTop.y != 255 || pixelTop.z != 255;
	}
	if (i + 1 < numRows){ // bottom
		if (threadIdx.x != blockDim.x - 1){// local bottom
			pixelBottom = sh_img[threadIdx.y + (threadIdx.x + 1) * blockDim.y];
		}
		else{
			pixelBottom = img[j + (i + 1) * numCols];
		}
		bottom = pixelBottom.x != 255 || pixelBottom.y != 255 || pixelBottom.z != 255;
	}
	if (j - 1 >= 0){ // left
		if (threadIdx.y != 0){ // local left
			pixelLeft = sh_img[(threadIdx.y - 1) + threadIdx.x * blockDim.y];
		}
		else{
			pixelLeft = img[(j - 1) + i * numCols];
		}
		left = pixelLeft.x != 255 || pixelLeft.y != 255 || pixelLeft.z != 255;
	}
	if (j + 1 < numCols){ // right
		if (threadIdx.y != blockDim.y - 1){ // local right
			pixelRight = sh_img[(threadIdx.y + 1) + threadIdx.x * blockDim.y];
		}
		else{
			pixelRight = img[(j + 1) + i * numCols];
		}
		right = pixelRight.x != 255 || pixelRight.y != 255 || pixelRight.z != 255;
	}

	if (inMask){
		if (top && bottom && left && right){
			maskInterior[idx_img] = 1;
			maskBorder[idx_img] = 0;
		}
		else{
			maskInterior[idx_img] = 0;
			maskBorder[idx_img] = 1;
		}
		mask[idx_img] = 1;
	}
	else{
		mask[idx_img] = 0;
		maskBorder[idx_img] = 0;
		maskInterior[idx_img] = 0;
	}
}

/***********************************
    Color Channel Manipulation
***********************************/
__global__ void splitChannels(const uchar4* const img, unsigned char* const red,
	unsigned char* const green, unsigned char* const blue, size_t numElems)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	if (idx >= numElems){ return; }
	red[idx] = img[idx].x;
	green[idx] = img[idx].y;
	blue[idx] = img[idx].z;
}

__global__ void copyUnsignedCharToFloat(const unsigned char* const charArray, float* const floatArray, size_t numElems)
{
	if (threadIdx.x + blockIdx.x * blockDim.x >= numElems){ return; }
	floatArray[threadIdx.x + blockIdx.x * blockDim.x] = (float)charArray[threadIdx.x + blockIdx.x * blockDim.x];
}

__global__ void combineChannels(uchar4* const img, const unsigned char* const red, 
	const unsigned char* const green, const unsigned char* const blue)
{
	uchar4 pixel;
	pixel.x = red[threadIdx.x + blockIdx.x * blockDim.x];
	pixel.y = green[threadIdx.x + blockIdx.x * blockDim.x];
	pixel.z = blue[threadIdx.x + blockIdx.x * blockDim.x];
	pixel.w = 255;
	img[threadIdx.x + blockIdx.x * blockDim.x] = pixel;
}

__global__ void flattenChannels_compact(const float* const sourceChannel_compact,
	const unsigned char* const destChannel,
	unsigned char* const blendedChannel, const int* const maskInterior,
	const int* const scanIndices, size_t numElems)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	if (idx >= numElems){ return; }
	if (maskInterior[idx] == 1){
		blendedChannel[idx] = (char)sourceChannel_compact[scanIndices[idx]];
	}
	else{
		blendedChannel[idx] = destChannel[idx];
	}
}

/***********************************
              Jacobi
***********************************/
__global__ void jacobiCompact(const float* const buffer_read, float* const buffer_write, 
	int* const buffer_compactIndices_top, int* const buffer_compactIndices_bottom,
	int* const buffer_compactIndices_left, int* const buffer_compactIndices_right,
	const int* const maskBorder, const int* const maskInterior,
	const unsigned char* const sourceChannel, const unsigned char* const destChannel,
	size_t numElems)
{
	int idx = threadIdx.x + blockDim.x * blockIdx.x;
	if (idx >= numElems){ return; }
	if (maskInterior[idx] != 1){ return; }

	float sum1 = 0.0, sum2 = 0.0;
	int denominator = 0;
	if (buffer_compactIndices_top[idx] >= 0){ // if top exists
		if (maskInterior[buffer_compactIndices_top[idx]] == 1){
			sum1 += buffer_read[buffer_compactIndices_top[idx]];
		}
		else if (maskBorder[buffer_compactIndices_top[idx]] == 1){
			sum1 += destChannel[buffer_compactIndices_top[idx]];
		}
		sum2 += (sourceChannel[idx] - sourceChannel[buffer_compactIndices_top[idx]]);
		denominator++;
	}
	if (buffer_compactIndices_bottom[idx] >= 0){ // bottom
		if (maskInterior[buffer_compactIndices_bottom[idx]] == 1){
			sum1 += buffer_read[buffer_compactIndices_bottom[idx]];
		}
		else if (maskBorder[buffer_compactIndices_bottom[idx]] == 1){
			sum1 += destChannel[buffer_compactIndices_bottom[idx]];
		}
		sum2 += (sourceChannel[idx] - sourceChannel[buffer_compactIndices_bottom[idx]]);
		denominator++;
	}
	if (buffer_compactIndices_left[idx] >= 0){ // left
		if (maskInterior[buffer_compactIndices_left[idx]] == 1){
			sum1 += buffer_read[buffer_compactIndices_left[idx]];
		}
		else if (maskBorder[buffer_compactIndices_left[idx]] == 1){
			sum1 += destChannel[buffer_compactIndices_left[idx]];
		}
		sum2 += (sourceChannel[idx] - sourceChannel[buffer_compactIndices_left[idx]]);
		denominator++;
	}
	if (buffer_compactIndices_right[idx] >= 0){ // right
		if (maskInterior[buffer_compactIndices_right[idx]] == 1){
			sum1 += buffer_read[buffer_compactIndices_right[idx]];
		}
		else if (maskBorder[buffer_compactIndices_right[idx]] == 1){
			sum1 += destChannel[buffer_compactIndices_right[idx]];
		}
		sum2 += (sourceChannel[idx] - sourceChannel[buffer_compactIndices_right[idx]]);
		denominator++;
	}

	float newValue = (sum1 + sum2) / denominator;
	buffer_write[idx] = fminf(255.f, fmaxf(0.f, newValue));
}

/***********************************
         Scan / Compact
***********************************/
__global__ void exclusiveScan(int* const scan, int* const scan_block, 
	const int * const orig_list, const int n, size_t numElems){
	int index_global = threadIdx.x + blockDim.x * blockIdx.x;
	int index_shared = threadIdx.x;

	// Use shared mem to reduce global read/writes
	extern __shared__ int sh_scan[];
	if (index_global >= numElems){
		sh_scan[index_shared] = 0;
	}
	else {
		sh_scan[index_shared] = scan[index_global];
	}
	__syncthreads();

	// n is the smallest power of 2 greater than or equal to blockDim.x
	for (int s = 1; s <= n / 2; s <<= 1){
		if (index_shared % (s + s) == ((blockDim.x - 1) % (s + s)) && index_shared - s >= 0){
			sh_scan[index_shared] = sh_scan[index_shared - s] + sh_scan[index_shared];
		}
		__syncthreads();
	}

	if (index_shared == blockDim.x - 1) sh_scan[index_shared] = 0;

	// n is the smallest power of 2 greater than or equal to blockDim.x
	int downsweep;
	for (int s = n / 2; s > 0; s >>= 1){
		if (index_shared % (s * 2) == ((blockDim.x - 1) % (s * 2))){
			if (index_shared - s >= 0){
				downsweep = sh_scan[index_shared];
				sh_scan[index_shared] = sh_scan[index_shared - s] + sh_scan[index_shared];
				sh_scan[index_shared - s] = downsweep;
			}
		}
		__syncthreads();
	}

	if (index_shared == blockDim.x - 1 || index_global == numElems - 1){
		scan_block[blockIdx.x] = sh_scan[index_shared] + orig_list[index_global];
	}
	if (index_global < numElems){
		scan[index_global] = sh_scan[index_shared];
	}
}

__global__ void addGridScan(int* const scan, int* const scan_block, size_t numElems){
	int index_global = threadIdx.x + blockDim.x * blockIdx.x;

	if (index_global >= numElems){
		return;
	}

	scan[index_global] += scan_block[blockIdx.x];
}

/***********************************
		Map to Compact
***********************************/
__global__ void compactMap(uchar4* const sourceImg_compact, const uchar4* const sourceImg_sparse, 
	uchar4* const destImg_compact, const uchar4* const destImg_sparse,
	int* const maskBorder_compact, const int* const maskBorder_sparse,
	int* const maskInterior_compact, const int* const maskInterior_sparse,
	const int* const indexScan, const int* const mask, int* const compactToOriginalIndices, 
	int* const compactToOriginalIndices_top, int* const compactToOriginalIndices_bottom,
	int* const compactToOriginalIndices_left, int* const compactToOriginalIndices_right,
	int* const compactIndices_top, int* const compactIndices_bottom,
	int* const compactIndices_left, int* const compactIndices_right,
	const size_t numRows, const size_t numCols)
{
	int idx_1d = threadIdx.x + blockIdx.x * blockDim.x;
	if (idx_1d >= numRows * numCols) { return; }
	if (mask[idx_1d] == 1){
		sourceImg_compact[indexScan[idx_1d]] = sourceImg_sparse[idx_1d];
		destImg_compact[indexScan[idx_1d]] = destImg_sparse[idx_1d];
		maskBorder_compact[indexScan[idx_1d]] = maskBorder_sparse[idx_1d];
		maskInterior_compact[indexScan[idx_1d]] = maskInterior_sparse[idx_1d];
		compactToOriginalIndices[indexScan[idx_1d]] = idx_1d;
		int i = idx_1d / numCols;
		int j = idx_1d % numCols;

		if (i - 1 >= 0){ // if top exists
			if (mask[j + (i - 1) * numCols] == 1){ // if top in compact
				compactIndices_top[indexScan[idx_1d]] = indexScan[j + (i - 1) * numCols];
			}
			else{
				compactIndices_top[indexScan[idx_1d]] = -2;
			}
			compactToOriginalIndices_top[indexScan[idx_1d]] = j + (i - 1) * numCols;
		}
		else{
			compactToOriginalIndices_top[indexScan[idx_1d]] = -1;
			compactIndices_top[indexScan[idx_1d]] = -1;
		}

		if (i + 1 < numRows){ // if bottom exists
			if (mask[j + (i + 1) * numCols] == 1){ // if bottom in compact
				compactIndices_bottom[indexScan[idx_1d]] = indexScan[j + (i + 1) * numCols];
			}
			else{
				compactIndices_bottom[indexScan[idx_1d]] = -2;
			}
			compactToOriginalIndices_bottom[indexScan[idx_1d]] = j + (i + 1) * numCols;
		}
		else{
			compactToOriginalIndices_bottom[indexScan[idx_1d]] = -1;
			compactIndices_bottom[indexScan[idx_1d]] = -1;
		}

		if (j - 1 >= 0){ // if left exists
			if (mask[(j - 1) + i * numCols] == 1){ // if left in compact
				compactIndices_left[indexScan[idx_1d]] = indexScan[(j - 1) + i * numCols];
			}
			else{
				compactIndices_left[indexScan[idx_1d]] = -2;
			}
			compactToOriginalIndices_left[indexScan[idx_1d]] = (j - 1) + i * numCols;
		}
		else{
			compactToOriginalIndices_left[indexScan[idx_1d]] = -1;
			compactIndices_left[indexScan[idx_1d]] = -1;
		}

		if (j + 1 < numCols){ // if right exists
			if (mask[(j + 1) + i * numCols] == 1){
				compactIndices_right[indexScan[idx_1d]] = indexScan[(j + 1) + i * numCols];
			}
			else{
				compactIndices_right[indexScan[idx_1d]] = -2;
			}
			compactToOriginalIndices_right[indexScan[idx_1d]] = (j + 1) + i * numCols;
		}
		else{
			compactToOriginalIndices_right[indexScan[idx_1d]] = -1;
			compactIndices_right[indexScan[idx_1d]] = -1;
		}
	}
}


/***********************************
           Driver Method
***********************************/
void your_blend(const uchar4* const h_sourceImg,  //IN
                const size_t numRowsSource, const size_t numColsSource,
                const uchar4* const h_destImg, //IN
                uchar4* const h_blendedImg) //OUT
{
	// Allocate / initialize
	uchar4 *d_sourceImg, *d_destImg, *d_blendedImg;
	unsigned char *d_destRed, *d_destGreen, *d_destBlue,*d_blendedRed, *d_blendedGreen, *d_blendedBlue;
	int *d_mask, *d_maskInterior, *d_maskBorder, *d_scanMask, *d_scanMask_block, *d_scanMask_block_placeholder;
	uchar4 *d_sourceImg_compact, *d_destImg_compact;
	int *d_maskInterior_compact, *d_maskBorder_compact;
	unsigned char *d_sourceRed_compact, *d_sourceGreen_compact, *d_sourceBlue_compact,
		*d_destRed_compact, *d_destGreen_compact, *d_destBlue_compact;
	int *d_compactToOriginalIndices, *d_compactToOriginalIndices_top, *d_compactToOriginalIndices_bottom,
		*d_compactToOriginalIndices_left, *d_compactToOriginalIndices_right,
		*d_compactToCompactIndices_top, *d_compactToCompactIndices_bottom,
		*d_compactToCompactIndices_left, *d_compactToCompactIndices_right;
	float *d_bufferRed_1_compact, *d_bufferGreen_1_compact, *d_bufferBlue_1_compact,
		*d_bufferRed_2_compact, *d_bufferGreen_2_compact, *d_bufferBlue_2_compact;

	checkCudaErrors(cudaMalloc(&d_sourceImg, sizeof(uchar4) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_destImg, sizeof(uchar4) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_blendedImg, sizeof(uchar4) * numRowsSource * numColsSource));

	checkCudaErrors(cudaMalloc(&d_mask, sizeof(int) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_scanMask, sizeof(int) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_scanMask_block_placeholder, sizeof(int) * 1));
	checkCudaErrors(cudaMalloc(&d_maskInterior, sizeof(int) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_maskBorder, sizeof(int) * numRowsSource * numColsSource));

	checkCudaErrors(cudaMalloc(&d_destRed, sizeof(unsigned char) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_destGreen, sizeof(unsigned char) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_destBlue, sizeof(unsigned char) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_blendedRed, sizeof(unsigned char) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_blendedGreen, sizeof(unsigned char) * numRowsSource * numColsSource));
	checkCudaErrors(cudaMalloc(&d_blendedBlue, sizeof(unsigned char) * numRowsSource * numColsSource));

	checkCudaErrors(cudaMemcpy(d_sourceImg, h_sourceImg, sizeof(uchar4) * numRowsSource * numColsSource, cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(d_destImg, h_destImg, sizeof(uchar4) * numRowsSource * numColsSource, cudaMemcpyHostToDevice));

	// Threads/blocks
	dim3 threads_2d(32, 32); // (i, j)
	dim3 blocks_2d((numRowsSource + 31) / 32, (numColsSource + 31) / 32); // (i, j)
	dim3 threads_1d(MAX_CUDA_THREADS);
	dim3 blocks_1d((numRowsSource * numColsSource + MAX_CUDA_THREADS - 1) / MAX_CUDA_THREADS);

	// Calculate mask
	calculateMask << <blocks_2d, threads_2d, 32 * 32 * sizeof(uchar4) >> >(d_sourceImg, d_mask, d_maskBorder, d_maskInterior, numRowsSource, numColsSource);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

	// Exclusive Blelloch Scan on Mask and then Compact
	checkCudaErrors(cudaMemcpy(d_scanMask, d_mask, sizeof(int) * numRowsSource * numColsSource, cudaMemcpyDeviceToDevice));
	checkCudaErrors(cudaMalloc(&d_scanMask_block, sizeof(int) * blocks_1d.x));
	int n = roundUpPower2(threads_1d.x);
	exclusiveScan << <blocks_1d, threads_1d, sizeof(int) * threads_1d.x >> >
		(d_scanMask, d_scanMask_block, d_mask, n, numRowsSource * numColsSource);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	n = roundUpPower2(blocks_1d.x);
	exclusiveScan << <1, blocks_1d, sizeof(int) * n>> >
		(d_scanMask_block, d_scanMask_block_placeholder, d_mask, n, blocks_1d.x);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	addGridScan << <blocks_1d, threads_1d >> >(d_scanMask, d_scanMask_block, numRowsSource * numColsSource);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

	// Calculate size of compacted source image
	thrust::device_ptr<int> d_ptr(d_mask);
	int numPixelsMask = thrust::reduce(d_ptr, d_ptr + (numRowsSource * numColsSource), (int)0, thrust::plus<int>());
	dim3 threads_1d_compact(MAX_CUDA_THREADS);
	dim3 blocks_1d_compact((numPixelsMask + MAX_CUDA_THREADS - 1) / MAX_CUDA_THREADS);

	// Allocate/initialize compacted arrays
	checkCudaErrors(cudaMalloc(&d_sourceImg_compact, sizeof(uchar4) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_destImg_compact, sizeof(uchar4) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToOriginalIndices, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToOriginalIndices_top, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToOriginalIndices_bottom, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToOriginalIndices_left, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToOriginalIndices_right, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToCompactIndices_top, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToCompactIndices_bottom, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToCompactIndices_left, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_compactToCompactIndices_right, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_maskInterior_compact, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_maskBorder_compact, sizeof(int) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_sourceRed_compact, sizeof(unsigned char) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_sourceGreen_compact, sizeof(unsigned char) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_sourceBlue_compact, sizeof(unsigned char) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_destRed_compact, sizeof(unsigned char) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_destGreen_compact, sizeof(unsigned char) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_destBlue_compact, sizeof(unsigned char) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_bufferRed_1_compact, sizeof(float) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_bufferGreen_1_compact, sizeof(float) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_bufferBlue_1_compact, sizeof(float) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_bufferRed_2_compact, sizeof(float) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_bufferGreen_2_compact, sizeof(float) * numPixelsMask));
	checkCudaErrors(cudaMalloc(&d_bufferBlue_2_compact, sizeof(float) * numPixelsMask));

	// Compact map
	compactMap << <blocks_1d, threads_1d >> >
		(d_sourceImg_compact, d_sourceImg, 
		d_destImg_compact, d_destImg,
		d_maskBorder_compact, d_maskBorder, 
		d_maskInterior_compact, d_maskInterior, 
		d_scanMask, d_mask, d_compactToOriginalIndices, 
		d_compactToOriginalIndices_top, d_compactToOriginalIndices_bottom, d_compactToOriginalIndices_left, d_compactToOriginalIndices_right, 
		d_compactToCompactIndices_top, d_compactToCompactIndices_bottom, d_compactToCompactIndices_left, d_compactToCompactIndices_right,
		numRowsSource, numColsSource);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

	// Split RGB channels
	splitChannels << <blocks_1d, threads_1d >> >
		(d_destImg, d_destRed, d_destGreen, d_destBlue, numRowsSource * numColsSource);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	splitChannels << <blocks_1d_compact, threads_1d_compact >> >
		(d_sourceImg_compact, d_sourceRed_compact, d_sourceGreen_compact, d_sourceBlue_compact, numPixelsMask);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	splitChannels << <blocks_1d_compact, threads_1d_compact >> >
		(d_destImg_compact, d_destRed_compact, d_destGreen_compact, d_destBlue_compact, numPixelsMask);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

	// Initialize Compact Buffers
	copyUnsignedCharToFloat << <blocks_1d_compact, threads_1d_compact >> > (d_sourceRed_compact, d_bufferRed_1_compact, numPixelsMask);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	copyUnsignedCharToFloat << <blocks_1d_compact, threads_1d_compact >> > (d_sourceGreen_compact, d_bufferGreen_1_compact, numPixelsMask);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	copyUnsignedCharToFloat << <blocks_1d_compact, threads_1d_compact >> > (d_sourceBlue_compact, d_bufferBlue_1_compact, numPixelsMask);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaMemcpy(d_bufferRed_2_compact, d_bufferRed_1_compact, sizeof(float) * numPixelsMask, cudaMemcpyDeviceToDevice));
	checkCudaErrors(cudaMemcpy(d_bufferGreen_2_compact, d_bufferGreen_1_compact, sizeof(float) * numPixelsMask, cudaMemcpyDeviceToDevice));
	checkCudaErrors(cudaMemcpy(d_bufferBlue_2_compact, d_bufferBlue_1_compact, sizeof(float) * numPixelsMask, cudaMemcpyDeviceToDevice));

	// Jacobi Compact
	for (int i = 0; i < 400; i++){
		jacobiCompact << <blocks_1d_compact, threads_1d_compact >> >
			(d_bufferRed_1_compact, d_bufferRed_2_compact, 
			d_compactToCompactIndices_top, d_compactToCompactIndices_bottom, d_compactToCompactIndices_left, d_compactToCompactIndices_right,
			d_maskBorder_compact, d_maskInterior_compact, d_sourceRed_compact, d_destRed_compact, numPixelsMask);
		cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
		jacobiCompact << <blocks_1d_compact, threads_1d_compact >> >
			(d_bufferGreen_1_compact, d_bufferGreen_2_compact, 
			d_compactToCompactIndices_top, d_compactToCompactIndices_bottom, d_compactToCompactIndices_left, d_compactToCompactIndices_right,
			d_maskBorder_compact, d_maskInterior_compact, d_sourceGreen_compact, d_destGreen_compact, numPixelsMask);
		cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
		jacobiCompact << <blocks_1d_compact, threads_1d_compact >> >
			(d_bufferBlue_1_compact, d_bufferBlue_2_compact,
			d_compactToCompactIndices_top, d_compactToCompactIndices_bottom, d_compactToCompactIndices_left, d_compactToCompactIndices_right,
			d_maskBorder_compact, d_maskInterior_compact, d_sourceBlue_compact, d_destBlue_compact, numPixelsMask);
		cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

		jacobiCompact << <blocks_1d_compact, threads_1d_compact >> >
			(d_bufferRed_2_compact, d_bufferRed_1_compact, 
			d_compactToCompactIndices_top, d_compactToCompactIndices_bottom, d_compactToCompactIndices_left, d_compactToCompactIndices_right,
			d_maskBorder_compact, d_maskInterior_compact, d_sourceRed_compact, d_destRed_compact, numPixelsMask);
		cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
		jacobiCompact << <blocks_1d_compact, threads_1d_compact >> >
			(d_bufferGreen_2_compact, d_bufferGreen_1_compact, 
			d_compactToCompactIndices_top, d_compactToCompactIndices_bottom, d_compactToCompactIndices_left, d_compactToCompactIndices_right,
			d_maskBorder_compact, d_maskInterior_compact, d_sourceGreen_compact, d_destGreen_compact, numPixelsMask);
		cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
		jacobiCompact << <blocks_1d_compact, threads_1d_compact >> >
			(d_bufferBlue_2_compact, d_bufferBlue_1_compact, 
			d_compactToCompactIndices_top, d_compactToCompactIndices_bottom, d_compactToCompactIndices_left, d_compactToCompactIndices_right,
			d_maskBorder_compact, d_maskInterior_compact, d_sourceBlue_compact, d_destBlue_compact, numPixelsMask);
		cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	}

	// Flatten color channels
	flattenChannels_compact << <blocks_1d, threads_1d >> >
		(d_bufferRed_1_compact, d_destRed, d_blendedRed, d_maskInterior, d_scanMask, numRowsSource * numColsSource);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	flattenChannels_compact << <blocks_1d, threads_1d >> >
		(d_bufferGreen_1_compact, d_destGreen, d_blendedGreen, d_maskInterior, d_scanMask, numRowsSource * numColsSource);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	flattenChannels_compact << <blocks_1d, threads_1d >> >
		(d_bufferBlue_1_compact, d_destBlue, d_blendedBlue, d_maskInterior, d_scanMask, numRowsSource * numColsSource);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

	// Combine RGB Channels, Write back to host
	combineChannels << <blocks_1d, threads_1d >> >(d_blendedImg, d_blendedRed, d_blendedGreen, d_blendedBlue);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaMemcpy(h_blendedImg, d_blendedImg, sizeof(uchar4) * numColsSource * numRowsSource, cudaMemcpyDeviceToHost));

	// Free Memory
	checkCudaErrors(cudaFree(d_sourceImg));
	checkCudaErrors(cudaFree(d_destImg));
	checkCudaErrors(cudaFree(d_destImg_compact));
	checkCudaErrors(cudaFree(d_blendedImg)); 
	checkCudaErrors(cudaFree(d_sourceImg_compact));
	checkCudaErrors(cudaFree(d_compactToOriginalIndices));
	checkCudaErrors(cudaFree(d_compactToOriginalIndices_top));
	checkCudaErrors(cudaFree(d_compactToOriginalIndices_bottom));
	checkCudaErrors(cudaFree(d_compactToOriginalIndices_left));
	checkCudaErrors(cudaFree(d_compactToOriginalIndices_right));
	checkCudaErrors(cudaFree(d_compactToCompactIndices_top));
	checkCudaErrors(cudaFree(d_compactToCompactIndices_bottom));
	checkCudaErrors(cudaFree(d_compactToCompactIndices_left));
	checkCudaErrors(cudaFree(d_compactToCompactIndices_right));

	checkCudaErrors(cudaFree(d_mask));	
	checkCudaErrors(cudaFree(d_maskInterior));
	checkCudaErrors(cudaFree(d_maskBorder));
	checkCudaErrors(cudaFree(d_maskInterior_compact));
	checkCudaErrors(cudaFree(d_maskBorder_compact));
	checkCudaErrors(cudaFree(d_scanMask));
	checkCudaErrors(cudaFree(d_scanMask_block));
	checkCudaErrors(cudaFree(d_scanMask_block_placeholder));

	checkCudaErrors(cudaFree(d_sourceRed_compact));
	checkCudaErrors(cudaFree(d_sourceGreen_compact));
	checkCudaErrors(cudaFree(d_sourceBlue_compact));
	
	checkCudaErrors(cudaFree(d_destRed));
	checkCudaErrors(cudaFree(d_destGreen));
	checkCudaErrors(cudaFree(d_destBlue));
	checkCudaErrors(cudaFree(d_destRed_compact));
	checkCudaErrors(cudaFree(d_destGreen_compact));
	checkCudaErrors(cudaFree(d_destBlue_compact));

	checkCudaErrors(cudaFree(d_blendedRed));
	checkCudaErrors(cudaFree(d_blendedGreen));
	checkCudaErrors(cudaFree(d_blendedBlue));

	checkCudaErrors(cudaFree(d_bufferRed_1_compact));
	checkCudaErrors(cudaFree(d_bufferGreen_1_compact));
	checkCudaErrors(cudaFree(d_bufferBlue_1_compact));
	checkCudaErrors(cudaFree(d_bufferRed_2_compact));
	checkCudaErrors(cudaFree(d_bufferGreen_2_compact));
	checkCudaErrors(cudaFree(d_bufferBlue_2_compact));

  /* To Recap here are the steps you need to implement
  
     1) Compute a mask of the pixels from the source image to be copied.
        The pixels that shouldn't be copied are completely white, they
        have R=255, G=255, B=255.  Any other pixels SHOULD be copied.
	
     2) Compute the interior and border regions of the mask.  An interior
        pixel has all 4 neighbors also inside the mask.  A border pixel is
        in the mask itself, but has at least one neighbor that isn't.

     3) Separate out the incoming image into three separate channels

     4) Create two float(!) buffers for each color channel that will
        act as our guesses.  Initialize them to the respective color
        channel of the source image since that will act as our intial guess.

     5) For each color channel perform the Jacobi iteration described 
        above 800 times.

     6) Create the output image by replacing all the interior pixels
        in the destination image with the result of the Jacobi iterations.
        Just cast the floating point values to unsigned chars since we have
        already made sure to clamp them to the correct range.

      Since this is final assignment we provide little boilerplate code to
      help you.  Notice that all the input/output pointers are HOST pointers.

      You will have to allocate all of your own GPU memory and perform your own
      memcopies to get data in and out of the GPU memory.

      Remember to wrap all of your calls with checkCudaErrors() to catch any
      thing that might go wrong.  After each kernel call do:

      cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

      to catch any errors that happened while executing the kernel.
  */
}
