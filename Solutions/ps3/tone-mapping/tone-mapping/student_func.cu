/* Udacity Homework 3
HDR Tone-mapping

Background HDR
==============

A High Dynamic Range (HDR) image contains a wider variation of intensity
and color than is allowed by the RGB format with 1 byte per channel that we
have used in the previous assignment.

To store this extra information we use single precision floating point for
each channel.  This allows for an extremely wide range of intensity values.

In the image for this assignment, the inside of church with light coming in
through stained glass windows, the raw input floating point values for the
channels range from 0 to 275.  But the mean is .41 and 98% of the values are
less than 3!  This means that certain areas (the windows) are extremely bright
compared to everywhere else.  If we linearly map this [0-275] range into the
[0-255] range that we have been using then most values will be mapped to zero!
The only thing we will be able to see are the very brightest areas - the
windows - everything else will appear pitch black.

The problem is that although we have cameras capable of recording the wide
range of intensity that exists in the real world our monitors are not capable
of displaying them.  Our eyes are also quite capable of observing a much wider
range of intensities than our image formats / monitors are capable of
displaying.

Tone-mapping is a process that transforms the intensities in the image so that
the brightest values aren't nearly so far away from the mean.  That way when
we transform the values into [0-255] we can actually see the entire image.
There are many ways to perform this process and it is as much an art as a
science - there is no single "right" answer.  In this homework we will
implement one possible technique.

Background Chrominance-Luminance
================================

The RGB space that we have been using to represent images can be thought of as
one possible set of axes spanning a three dimensional space of color.  We
sometimes choose other axes to represent this space because they make certain
operations more convenient.

Another possible way of representing a color image is to separate the color
information (chromaticity) from the brightness information.  There are
multiple different methods for doing this - a common one during the analog
television days was known as Chrominance-Luminance or YUV.

We choose to represent the image in this way so that we can remap only the
intensity channel and then recombine the new intensity values with the color
information to form the final image.

Old TV signals used to be transmitted in this way so that black & white
televisions could display the luminance channel while color televisions would
display all three of the channels.


Tone-mapping
============

In this assignment we are going to transform the luminance channel (actually
the log of the luminance, but this is unimportant for the parts of the
algorithm that you will be implementing) by compressing its range to [0, 1].
To do this we need the cumulative distribution of the luminance values.

Example
-------

input : [2 4 3 3 1 7 4 5 7 0 9 4 3 2]
min / max / range: 0 / 9 / 9

histo with 3 bins: [4 7 3]

cdf : [4 11 14]


Your task is to calculate this cumulative distribution by following these
steps.

*/


#include "reference_calc.cpp"
#include "utils.h"
#include <stdio.h>
#include <cmath>
#include <algorithm>
const int maxThreadsPerBlock = 1024;
const int maxBytesPerSharedMem = 32768;
const int localArraySize = 4096;

// Taken from stackoverflow
int roundUpPower2(int n)
{
	n--;           // 1000 0011 --> 1000 0010
	n |= n >> 1;   // 1000 0010 | 0100 0001 = 1100 0011
	n |= n >> 2;   // 1100 0011 | 0011 0000 = 1111 0011
	n |= n >> 4;   // 1111 0011 | 0000 1111 = 1111 1111
	n |= n >> 8;   // ... (At this point all bits are 1, so further bitwise-or
	n |= n >> 16;  //      operations produce no effect.)
	n++;           // 1111 1111 --> 1 0000 0000
	return n;
}

/////////////////////////////////////
//           Min / Max
////////////////////////////////////
__device__ void warpReduceMin(volatile float *sdata, int threadId_block)
{
	sdata[threadId_block] = min(sdata[threadId_block], sdata[threadId_block + 32]);
	sdata[threadId_block] = min(sdata[threadId_block], sdata[threadId_block + 16]);
	sdata[threadId_block] = min(sdata[threadId_block], sdata[threadId_block + 8]);
	sdata[threadId_block] = min(sdata[threadId_block], sdata[threadId_block + 4]);
	sdata[threadId_block] = min(sdata[threadId_block], sdata[threadId_block + 2]);
	sdata[threadId_block] = min(sdata[threadId_block], sdata[threadId_block + 1]);
}

__device__ void warpReduceMax(volatile float *sdata, int threadId_block)
{
	sdata[threadId_block] = max(sdata[threadId_block], sdata[threadId_block + 32]);
	sdata[threadId_block] = max(sdata[threadId_block], sdata[threadId_block + 16]);
	sdata[threadId_block] = max(sdata[threadId_block], sdata[threadId_block + 8]);
	sdata[threadId_block] = max(sdata[threadId_block], sdata[threadId_block + 4]);
	sdata[threadId_block] = max(sdata[threadId_block], sdata[threadId_block + 2]);
	sdata[threadId_block] = max(sdata[threadId_block], sdata[threadId_block + 1]);
}

__global__ void reduceMin(float* const d_out,
	const float* const d_in,
	const unsigned int n){

	unsigned int index_global = threadIdx.x + (blockDim.x * blockIdx.x * 2);
	unsigned int threadId_block = threadIdx.x;

	extern __shared__ float sdata[];
	// Load shared mem from global mem
	sdata[threadId_block] = min(d_in[index_global], d_in[index_global + blockDim.x]);
	// Make sure entire thread is loaded
	__syncthreads();

	// n is the smallest power of 2 greater than or equal to blockDim.x
	for (unsigned int s = n / 2; s > 32; s >>= 1){
		if (threadId_block < s && threadId_block + s < blockDim.x){
			sdata[threadId_block] = min(sdata[threadId_block + s], sdata[threadId_block]);
		}
		__syncthreads();
	}

	if (threadId_block < 32) warpReduceMin(sdata, threadId_block);
	if (threadId_block == 0){
		d_out[blockIdx.x] = sdata[0];
	}
}

__global__ void reduceMax(float* const d_out,
	const float* const d_in,
	const unsigned int n){

	unsigned int index_global = threadIdx.x + (blockDim.x * blockIdx.x * 2);
	unsigned int threadId_block = threadIdx.x;

	extern __shared__ float sdata[];
	// Load shared mem from global mem
	sdata[threadId_block] = max(d_in[index_global], d_in[index_global + blockDim.x]);
	// Make sure entire thread is loaded
	__syncthreads();

	// n is the smallest power of 2 greater than or equal to blockDim.x
	for (unsigned int s = n / 2; s > 32; s >>= 1){
		if (threadId_block < s && threadId_block + s < blockDim.x){
			sdata[threadId_block] = max(sdata[threadId_block + s], sdata[threadId_block]);
		}
		__syncthreads();
	}

	if (threadId_block < 32) warpReduceMax(sdata, threadId_block);
	if (threadId_block == 0){
		d_out[blockIdx.x] = sdata[0];
	}
}

void calcMinMax(const float* const d_logLuminance,
	float &min_logLum,
	float &max_logLum,
	const size_t numRows,
	const size_t numCols)
{
	float *d_min_in, *d_min_out;
	float *d_max_in, *d_max_out;
	int threads, blocks, n, reductionElements;
	checkCudaErrors(cudaMalloc(&d_min_in, sizeof(float) * numRows * numCols));
	checkCudaErrors(cudaMemcpy(d_min_in, d_logLuminance, sizeof(float) * numRows * numCols, cudaMemcpyDeviceToDevice));
	checkCudaErrors(cudaMalloc(&d_max_in, sizeof(float) * numRows * numCols));
	checkCudaErrors(cudaMemcpy(d_max_in, d_logLuminance, sizeof(float) * numRows * numCols, cudaMemcpyDeviceToDevice));

	// Each loop iteration cuts the problem down by a factor of
	// maxThreadsPerBlock. Output is passed back to the input after
	// each reduction
	reductionElements = numRows * numCols;
	while (reductionElements > 1){
		threads = std::min(maxThreadsPerBlock, reductionElements); 
		blocks = reductionElements <= maxThreadsPerBlock ? 1 : (reductionElements + threads - 1) / threads / 2;
		n = roundUpPower2(threads);

		checkCudaErrors(cudaMalloc(&d_min_out, sizeof(float) * blocks));
		checkCudaErrors(cudaMalloc(&d_max_out, sizeof(float) * blocks));
		reduceMin << <blocks, threads, sizeof(float) * threads >> >(d_min_out, d_min_in, n);
		reduceMax << <blocks, threads, sizeof(float) * threads >> >(d_max_out, d_max_in, n);

		reductionElements = blocks;

		cudaFree(d_min_in);
		cudaFree(d_max_in);
		checkCudaErrors(cudaMalloc(&d_min_in, sizeof(float) * reductionElements));
		checkCudaErrors(cudaMemcpy(d_min_in, d_min_out, sizeof(float) * reductionElements, cudaMemcpyDeviceToDevice));
		checkCudaErrors(cudaMalloc(&d_max_in, sizeof(float) * reductionElements));
		checkCudaErrors(cudaMemcpy(d_max_in, d_max_out, sizeof(float) * reductionElements, cudaMemcpyDeviceToDevice));
		cudaFree(d_max_out);
		cudaFree(d_min_out);
	}		

	// Bring back final value to host
	float *h_min, *h_max;	
	h_min = new float[blocks];
	h_max = new float[blocks];		
	checkCudaErrors(cudaMemcpy(h_min, d_min_in, sizeof(float) * blocks, cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(h_max, d_max_in, sizeof(float) * blocks, cudaMemcpyDeviceToHost));
	max_logLum = h_max[0];
	min_logLum = h_min[0];		

	// Free memory
	delete[] h_min;
	delete[] h_max;
	cudaFree(d_min_in);
	cudaFree(d_max_in);
}

/////////////////////////////////////
//           Histogram
////////////////////////////////////

// This method only works if # bins == # max threads
__global__ void atomicHistogramShmem(int* const d_bins,
	const float* const d_in,
	const float min_logLum,
	const float range_logLum,
	const size_t numBins)
{
	int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
	
	//Initialize shared mem histogram and sync
	extern __shared__ int sh_bins[];
	sh_bins[threadIdx.x] = 0;
	__syncthreads();

	//Calc bin
	float pixel_logLum = d_in[threadId_global];
	int bin = (int)round((pixel_logLum - min_logLum) / range_logLum  * (numBins - 1));

	//Increment shared histogram and sync
	atomicAdd(&(sh_bins[bin]), 1);
	__syncthreads();

	//To global histogram, add shared histogram bin totals
	//using thread id as an indexer
	atomicAdd(&(d_bins[threadIdx.x]), sh_bins[threadIdx.x]);
}

__global__ void initializeArray(int* d_in, const int val)
{
	int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
	d_in[threadId_global] = val;
}

void createHistogram(int* d_bins, const float* const d_logLuminance,
	const size_t numRows, const size_t numCols, const size_t numBins,
	const float min_logLum, const float range_logLum)
{
	int threads_initialization = maxThreadsPerBlock;
	int blocks_initialization = numBins / threads_bins;
	initializeArray << <blocks_initialization, threads_initialization >> >(d_bins, 0);

	int threads_pixels = numBins;
	int blocks_pixels = (numRows * numCols + threads_pixels - 1) / threads_pixels;	
	atomicHistogramShmem << <blocks_pixels, threads_pixels, sizeof(int) * numBins>> >(d_bins, d_logLuminance, min_logLum, range_logLum, numBins);
}

/////////////////////////////////////
//          Scan / CDF
////////////////////////////////////



// Hillis Steele Scan
__global__ void inclusiveScan(unsigned int* const d_in, const int n)
{
	int threadId_global = threadIdx.x + blockDim.x * blockIdx.x;
	int threadId_block = threadIdx.x;

	extern __shared__ unsigned int shIncScan[];
	shIncScan[threadId_block] = d_in[threadId_global];
	__syncthreads();
	unsigned int sum;

	// n is the smallest power of 2 greater than or equal to blockDim.x
	for (int s = 1; s <= n / 2; s <<= 1){			
		sum = 0;
		if (threadId_block - s >= 0){
			sum = shIncScan[threadId_block - s] + shIncScan[threadId_block];
		}
		__syncthreads();
		if (threadId_block - s >= 0){
			shIncScan[threadId_block] = sum;
		}
	}

	d_in[threadId_global] = shIncScan[threadId_block];
}

__global__ void inclusiveScanShift(unsigned int *scan, int *shift)
{
	int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
	scan[threadId_global] -= shift[threadId_global];
}

// Uses Hillis Steele and a conversion
void exclusiveScan(unsigned int* d_out, int* const d_in, const size_t numBins)
{
	checkCudaErrors(cudaMemcpy(d_out, d_in, sizeof(unsigned int) * numBins, cudaMemcpyDeviceToDevice));

	int threads_histogram = std::min(maxThreadsPerBlock, (int)numBins);
	int blocks_histogram = (numBins + threads_histogram - 1) / threads_histogram;
	int n = roundUpPower2(threads_histogram);
	inclusiveScan << <blocks_histogram, threads_histogram, sizeof(int) * numBins>> >(d_out, n);
	inclusiveScanShift << <blocks_histogram, threads_histogram >> >(d_out, d_in);
}

/////////////////////////////////////
//        Driver Method
////////////////////////////////////

void your_histogram_and_prefixsum(const float* const d_logLuminance,
	unsigned int* const d_cdf,
	float &min_logLum,
	float &max_logLum,
	const size_t numRows,
	const size_t numCols,
	const size_t numBins)
{
	/*Here are the steps you need to implement
	1) find the minimum and maximum value in the input logLuminance channel
	store in min_logLum and max_logLum
	2) subtract them to find the range
	3) generate a histogram of all the values in the logLuminance channel using
	the formula: bin = (lum[i] - lumMin) / lumRange * numBins
	4) Perform an exclusive scan (prefix sum) on the histogram to get
	the cumulative distribution of luminance values (this should go in the
	incoming d_cdf pointer which already has been allocated for you)       */

	calcMinMax(d_logLuminance, min_logLum, max_logLum, numRows, numCols);
	float range_logLum = max_logLum - min_logLum;
	int *d_bins;
	checkCudaErrors(cudaMalloc(&d_bins, sizeof(int) * numBins));
	createHistogram(d_bins, d_logLuminance, numRows, numCols, numBins, min_logLum, range_logLum);
	exclusiveScan(d_cdf, d_bins, numBins);
	cudaFree(d_bins);
}


/////////////////////////////////////////////////////
//    Graveyard - may want some of this for later
/////////////////////////////////////////////////////

/*
void reduceMinMax(const float* const d_logLuminance,
	float &min_logLum,
	float &max_logLum,
	const size_t numRows,
	const size_t numCols,
	int isMax)
{
	float *d_minmax_in, *d_minmax_intermediate, *d_minmax;

	// STEP 1: Thread/Block size
	const int maxThreadsPerBlock = 1024;
	int threads = maxThreadsPerBlock;
	int blocks = numRows * numCols / threads;
	int n = roundUpPower2(threads);

	// STEP 1: Kernel
	checkCudaErrors(cudaMalloc(&d_minmax_in, sizeof(float) * numRows * numCols));
	checkCudaErrors(cudaMemcpy(d_minmax_in, d_logLuminance, sizeof(float) * numRows * numCols, cudaMemcpyDeviceToDevice));
	checkCudaErrors(cudaMalloc(&d_minmax_intermediate, sizeof(float) * blocks));
	parallelMinMax_shmem << <blocks, threads, sizeof(float) * threads >> >(d_minmax_intermediate, d_minmax_in, n, isMax);

	// STEP 1: Print intermediate reductions
	// float *h_minmax_intermediate;
	// h_minmax_intermediate = new float[blocks];
	// checkCudaErrors(cudaMemcpy(h_minmax_intermediate, d_minmax_intermediate,   sizeof(float) * blocks, cudaMemcpyDeviceToHost));
	// for(int i = 0; i < blocks; i++){
	//     printf("Block %d Max: %f\n", i, h_minmax_intermediate[i]);
	// }

	// STEP 2: Reset thread/block size
	threads = blocks;
	blocks = 1;
	n = roundUpPower2(threads);

	// STEP 2: Kernel
	checkCudaErrors(cudaMalloc(&d_minmax, sizeof(float) * blocks));
	parallelMinMax_shmem << <blocks, threads, sizeof(float) * threads >> >(d_minmax, d_minmax_intermediate, n, isMax);

	// STEP 2: Bring back final value to host
	float *h_minmax;
	h_minmax = new float[blocks];
	checkCudaErrors(cudaMemcpy(h_minmax, d_minmax, sizeof(float) * blocks, cudaMemcpyDeviceToHost));
	// printf("Final Max Value: %f\n", h_minmax[0]);

	if (isMax != 0){
		max_logLum = h_minmax[0];
	}
	else{
		min_logLum = h_minmax[0];
	}

	// Free memory
	delete[] h_minmax;
	// delete[] h_minmax_intermediate;
	cudaFree(d_minmax_intermediate);
	cudaFree(d_minmax);
}

void calcMinMax(const float* const d_logLuminance,
float &min_logLum,
float &max_logLum,
const size_t numRows,
const size_t numCols)
{
float *d_min_in, *d_min_intermediate, *d_min;
float *d_max_in, *d_max_intermediate, *d_max;

// STEP 1: Thread/Block size
const int maxThreadsPerBlock = 1024;
int threads = maxThreadsPerBlock;
int blocks = numRows * numCols / threads;
int n = roundUpPower2(threads);

// STEP 1: Kernel
checkCudaErrors(cudaMalloc(&d_min_in, sizeof(float) * numRows * numCols));
checkCudaErrors(cudaMemcpy(d_min_in, d_logLuminance, sizeof(float) * numRows * numCols, cudaMemcpyDeviceToDevice));
checkCudaErrors(cudaMalloc(&d_min_intermediate, sizeof(float) * blocks));
checkCudaErrors(cudaMalloc(&d_max_in, sizeof(float) * numRows * numCols));
checkCudaErrors(cudaMemcpy(d_max_in, d_logLuminance, sizeof(float) * numRows * numCols, cudaMemcpyDeviceToDevice));
checkCudaErrors(cudaMalloc(&d_max_intermediate, sizeof(float) * blocks));
reduceMin << <blocks, threads, sizeof(float) * threads >> >(d_min_intermediate, d_min_in, n);
reduceMax << <blocks, threads, sizeof(float) * threads >> >(d_max_intermediate, d_max_in, n);

// STEP 2: Reset thread/block size
threads = blocks;
blocks = 1;
n = roundUpPower2(threads);

// STEP 2: Kernel
checkCudaErrors(cudaMalloc(&d_min, sizeof(float) * blocks));
checkCudaErrors(cudaMalloc(&d_max, sizeof(float) * blocks));
reduceMin << <blocks, threads, sizeof(float) * threads >> >(d_min, d_min_intermediate, n);
reduceMin << <blocks, threads, sizeof(float) * threads >> >(d_max, d_max_intermediate, n);

// STEP 2: Bring back final value to host
float *h_min, *h_max;
h_min = new float[blocks];
h_max = new float[blocks];
checkCudaErrors(cudaMemcpy(h_min, d_min, sizeof(float) * blocks, cudaMemcpyDeviceToHost));
checkCudaErrors(cudaMemcpy(h_max, d_max, sizeof(float) * blocks, cudaMemcpyDeviceToHost));

max_logLum = h_max[0];
min_logLum = h_min[0];


// Free memory
delete[] h_min;
delete[] h_max;
cudaFree(d_min_intermediate);
cudaFree(d_max_intermediate);
cudaFree(d_max);
cudaFree(d_min);
}

__global__ void parallelMinMax_shmem(float* const d_out,
const float* const d_in,
const unsigned int n,
int isMax){
int threadId_global = threadIdx.x + blockDim.x * blockIdx.x;
int threadId_block = threadIdx.x;

extern __shared__ float sdata[];

// load shared mem from global mem
sdata[threadId_block] = d_in[threadId_global];
//make sure entire thread is loaded
__syncthreads();

// n is the smallest power of 2 greater than or equal to blockDim.x
for (unsigned int s = n / 2; s > 0; s >>= 1){
if (threadId_block < s){
if (threadId_block + s < blockDim.x){
//printf("%d/%d/%d: (%d, %f) (%d, %f) -> (%d, %f)\n", s, blockDim.x, n, threadId_block, sdata[threadId_block], threadId_block + s, sdata[threadId_block + s], threadId_block, max(sdata[threadId_block + s], sdata[threadId_block]));
if (isMax != 0){
sdata[threadId_block] = max(sdata[threadId_block + s], sdata[threadId_block]);
}
else{
sdata[threadId_block] = min(sdata[threadId_block + s], sdata[threadId_block]);
}
}
}
__syncthreads();
}

if (threadId_block == 0){
//printf("%d/%d: %f\n", threadId_block, n, sdata[0]);
d_out[blockIdx.x] = sdata[0];
}
}


__global__ void atomicHistogram(int* const d_bins,
const float* const d_in,
const float min_logLum,
const float range_logLum,
const size_t numBins)
{
int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
float pixel_logLum = d_in[threadId_global];
int bin = (int)round((pixel_logLum - min_logLum) / range_logLum  * (numBins - 1));
atomicAdd(&(d_bins[bin]), 1);
}


__global__ void localHistograms(int* const d_bins_local,
const float* const d_in,
const float min_logLum,
const float range_logLum,
const size_t numBins,
const int localArraySize)
{
int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
int baseIndex_in = threadId_global * localArraySize;
int baseIndex_out = threadId_global * numBins;
int index, bin;
float pixel_logLum;
for (int i = 0; i < localArraySize; i++){
pixel_logLum = d_in[baseIndex_in + i];
bin = (int)floor((pixel_logLum - min_logLum) / range_logLum * numBins);
d_bins_local[bin + baseIndex_out] += 1;
}
}

__global__ void reduceHistograms(int* const d_out,
const int* const d_in, const unsigned int n, const int numBins, int pass)
{
int threadId_global = threadIdx.x + blockDim.x * blockIdx.x; // Which histogram #
int threadId_block = threadIdx.x; // Which histogram # on this block

extern __shared__ float sdata[];
// Load shared mem from global mem
for (int j = 0; j < numBins; j++){
sdata[threadId_block * numBins + j] = d_in[threadId_global * numBins + j];
}
// Make sure entire thread is loaded
__syncthreads();

// n is the smallest power of 2 greater than or equal to blockDim.x
for (unsigned int s = n / 2; s > 0; s >>= 1){
if (threadId_block < s && threadId_block + s < blockDim.x){
for (int i = 0; i < numBins; i++){
sdata[threadId_block * numBins + i] += sdata[(threadId_block + s) * numBins + i];
}
}
__syncthreads();
}

if (threadId_block == 0){
for (int i = 0; i < numBins; i++){
d_out[blockIdx.x * numBins + i] = sdata[0 + i];
}
}
}

void calcLocalHistograms(int *d_bins, int *d_bins_local, const int count_d_bins_local, const int numBins)
{
int *d_bins_local_in, *d_bins_local_out;
int threads, blocks, n, reductionElements, arraySize, maxThreads, shMemMaxThreads;
shMemMaxThreads = maxBytesPerSharedMem / sizeof(int) / numBins;
maxThreads = std::min(maxThreadsPerBlock, shMemMaxThreads);
checkCudaErrors(cudaMalloc(&d_bins_local_in, sizeof(int) * count_d_bins_local));
checkCudaErrors(cudaMemcpy(d_bins_local_in, d_bins_local, sizeof(int) * count_d_bins_local, cudaMemcpyDeviceToDevice));

// Each loop iteration cuts the problem down by a factor of
// maxThreadsPerBlock. Output is passed back to the input after
// each reduction
reductionElements = count_d_bins_local / numBins; // number of distinct histograms
int i = 0;
while (reductionElements > 1){
threads = std::min(maxThreads, reductionElements);
blocks = (reductionElements + threads - 1) / threads;
n = roundUpPower2(threads);

checkCudaErrors(cudaMalloc(&d_bins_local_out, sizeof(int) * blocks * numBins));
reduceHistograms << <blocks, threads, sizeof(int) * threads * numBins >> >(d_bins_local_out, d_bins_local_in, n, numBins, i);

reductionElements = blocks;

cudaFree(d_bins_local_in);
arraySize = sizeof(int) * reductionElements * numBins;
checkCudaErrors(cudaMalloc(&d_bins_local_in, arraySize));
checkCudaErrors(cudaMemcpy(d_bins_local_in, d_bins_local_out, arraySize, cudaMemcpyDeviceToDevice));
cudaFree(d_bins_local_out);
}

// Copy values into d_bins
checkCudaErrors(cudaMemcpy(d_bins, d_bins_local_in, sizeof(int) * numBins, cudaMemcpyDeviceToDevice));
cudaFree(d_bins_local_in);
}

__global__ void atomicHistogramLocalized(int* const d_bins_intermediate,
const float* const d_in,
const float min_logLum,
const float range_logLum,
const size_t numBins)
{
int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
int baseIndexOutput = blockIdx.x * numBins;
float pixel_logLum = d_in[threadId_global];
int bin = (int)round((pixel_logLum - min_logLum) / range_logLum  * (numBins - 1));
atomicAdd(&(d_bins_intermediate[bin + baseIndexOutput]), 1);
}

__global__ void atomicHistogramReduction(int* const d_bins,
int* const d_bins_intermediate,	const size_t numBins)
{
int intermediateBin = threadIdx.x + blockIdx.x * blockDim.x;
int bin = intermediateBin % numBins;
atomicAdd(&(d_bins[bin]), d_bins_intermediate[intermediateBin]);
}

void createHistogram(int* d_bins, const float* const d_logLuminance,
const size_t numRows, const size_t numCols, const size_t numBins,
const float min_logLum, const float max_logLum, const float range_logLum)
{
int threads_bins = maxThreadsPerBlock;
int blocks_bins = numBins / threads_bins;
initializeArray << <blocks_bins, threads_bins >> >(d_bins, 0);

int threads_pixels = numBins;
int blocks_pixels = (numRows * numCols + threads_pixels - 1) / threads_pixels;
atomicHistogramShmem << <blocks_pixels, threads_pixels, sizeof(int) * numBins>> >(d_bins, d_logLuminance, min_logLum, max_logLum, range_logLum, numBins);
//atomicHistogram << <blocks_pixels, threads_pixels>> >(d_bins, d_logLuminance, min_logLum, range_logLum, numBins);
//int *d_bins_intermediate;
//checkCudaErrors(cudaMalloc(&d_bins_intermediate, sizeof(int) * numBins * blocks_pixels));
//atomicHistogramLocalized << <blocks_pixels, threads_pixels >> >(d_bins_intermediate, d_logLuminance, min_logLum, range_logLum, numBins);
//int threads_reduction = std::min(maxThreadsPerBlock, (int)(blocks_pixels * numBins));
//int blocks_reduction = (numBins * blocks_pixels + threads_reduction - 1) / threads_reduction;
//atomicHistogramReduction << <blocks_reduction, threads_reduction >> >(d_bins, d_bins_intermediate, numBins);

//int totalThreads_localHistogram = numRows * numCols / localArraySize;
//int threads_localHistogram = std::min(maxThreadsPerBlock, totalThreads_localHistogram);
//int blocks_localHistogram = totalThreads_localHistogram / threads_localHistogram;

//int *d_bins_local; // d_bins but with one for each thread
//int count_d_bins_local = numBins * totalThreads_localHistogram;
//checkCudaErrors(cudaMalloc(&d_bins_local, sizeof(int) * count_d_bins_local));
//int threads_bins_local = std::min(maxThreadsPerBlock, count_d_bins_local);
//int blocks_bins_local = count_d_bins_local / threads_bins_local;

//initializeArray << <blocks_bins_local, threads_bins_local >> >(d_bins_local, 0);
//localHistograms << <blocks_localHistogram, threads_localHistogram >> >(d_bins_local, d_logLuminance, min_logLum, range_logLum, numBins, localArraySize);

//int *h_bins_local, *h_bins;
//h_bins_local = new int[count_d_bins_local];
//h_bins = new int[numBins];
//checkCudaErrors(cudaMemcpy(h_bins_local, d_bins_local, sizeof(int) * count_d_bins_local, cudaMemcpyDeviceToHost));
//for (int i = 0; i < numBins; i++){
//	h_bins[i] = 0;
//}
//for (int i = 0; i < count_d_bins_local; i++){
//	//if (i % numBins == 0){
//	//	printf("%d --------------------------\n", (i / numBins));
//	//}
//	//printf("%d\n", h_bins_local[i]);
//	int bin = i % 1024;
//	h_bins[bin] += h_bins_local[i];
//}
//for (int i = 0; i < numBins; i++){
//	printf("%d %d\n", i, h_bins[i]);
//}
//delete[] h_bins_local;

//calcLocalHistograms(d_bins, d_bins_local, count_d_bins_local, numBins);
//int *h_bins;
//h_bins = new int[numBins];
//checkCudaErrors(cudaMemcpy(h_bins, d_bins, sizeof(int) * numBins, cudaMemcpyDeviceToHost));
//for (int i = 0; i < numBins; i++){
//	printf("%d %d\n", i, h_bins[i]);
//}
//delete[] h_bins;
//reduceHistogram << <blocks_localHistogram, threads_localHistogram >> >(d_bins, d_bins_local);
//cudaFree(d_bins_local);
}




// Blelloch Scan
void exclusiveScan(unsigned int* d_out, int* const d_in, const size_t numBins){

//unsigned int *d_intermediate;
//checkCudaErrors(cudaMalloc(&d_intermediate, sizeof(int) * numBins));
checkCudaErrors(cudaMemcpy(d_out, d_in, sizeof(int) * numBins, cudaMemcpyDeviceToDevice));

int threads_histogram = std::min(maxThreadsPerBlock, (int) numBins);
int blocks_histogram = (numBins + threads_histogram - 1) / threads_histogram;
int n = roundUpPower2(threads_histogram);
exclusiveScan_reduction << <blocks_histogram, threads_histogram, sizeof(int) * numBins >> >(d_out, n);

//int *h_intermediate;
//h_intermediate = new int[numBins];
//checkCudaErrors(cudaMemcpy(h_intermediate, d_intermediate, sizeof(int) * numBins, cudaMemcpyDeviceToHost));
//for(int i = 0; i < numBins; i++){
//    printf("int %d: %d\n", i, h_intermediate[i]);
//}
//delete[] h_intermediate;

//d_intermediate[numBins - 1] = 0;
initializeDownsweep << <1, 1 >> >(d_out, numBins - 1);
exclusiveScan_downsweep << <blocks_histogram, threads_histogram, sizeof(int) * numBins >> >(d_out, n);
//checkCudaErrors(cudaMemcpy(d_out, d_intermediate, sizeof(int) * numBins, cudaMemcpyDeviceToDevice));

//int *h_out;
//h_out = new int[numBins];
//checkCudaErrors(cudaMemcpy(h_out, d_out, sizeof(int) * numBins, cudaMemcpyDeviceToHost));
//for(int i = 0; i < numBins; i++){
//    printf("out %d: %d\n", i, h_out[i]);
//}
//delete[] h_out;

//cudaFree(d_intermediate);
}


// Blelloch Scan
__global__ void exclusiveScan_reduction(unsigned int* const d_in, int n){
int threadId_global = threadIdx.x + blockDim.x * blockIdx.x;
int threadId_block = threadIdx.x;

extern __shared__ unsigned int shscan[];
shscan[threadId_block] = d_in[threadId_global];
__syncthreads();

// n is the smallest power of 2 greater than or equal to blockDim.x
for (int s = 1; s <= n / 2; s <<= 1){
if (threadId_global % (s + s) == ((blockDim.x - 1) % (s + s)) && threadId_global - s >= 0){
//d_in[threadId_global] = d_in[threadId_global - s] + d_in[threadId_global];
shscan[threadId_global] = shscan[threadId_global - s] + shscan[threadId_global];
}
__syncthreads();
}

d_in[threadId_global] = shscan[threadId_block];
}

// Blelloch Scan
__global__ void exclusiveScan_downsweep(unsigned int* d_in, int n){
int threadId_global = threadIdx.x + blockDim.x * blockIdx.x;
int threadId_block = threadIdx.x;

extern __shared__ unsigned int shds[];
shds[threadId_block] = d_in[threadId_global];
__syncthreads();

// n is the smallest power of 2 greater than or equal to blockDim.x
for (int s = n / 2; s > 0; s >>= 1){
if (threadId_block % (s * 2) == ((blockDim.x - 1) % (s * 2))){
if (threadId_block - s >= 0){
int downsweep = shds[threadId_block];
shds[threadId_block] = shds[threadId_block - s] + shds[threadId_block];
shds[threadId_block - s] = downsweep;
}
}
__syncthreads();
}

d_in[threadId_global] = shds[threadId_block];
}

//unsigned int* d_cdf_inclusive;
//checkCudaErrors(cudaMalloc(&d_cdf_inclusive, sizeof(int) * numBins));
//exclusiveScan(d_cdf, d_bins, numBins);

//unsigned int *h_cdf, *h_cdf_inclusive;
//int *h_bins;
//h_cdf = new unsigned int[numBins];
//h_cdf_inclusive = new unsigned int[numBins];
//h_bins = new int[numBins];
//checkCudaErrors(cudaMemcpy(h_cdf, d_cdf, sizeof(unsigned int) * numBins, cudaMemcpyDeviceToHost));
//checkCudaErrors(cudaMemcpy(h_cdf_inclusive, d_cdf_inclusive, sizeof(unsigned int) * numBins, cudaMemcpyDeviceToHost));
//checkCudaErrors(cudaMemcpy(h_bins, d_bins, sizeof(int) * numBins, cudaMemcpyDeviceToHost));
//for (int i = 0; i < numBins; i++){
//	printf("%d %d %u  %u\n", i, h_bins[i], h_cdf[i], h_cdf_inclusive[i]);
//}
//
//delete[] h_bins;
//delete[] h_cdf;
//delete[] h_cdf_inclusive;
//cudaFree(d_cdf_inclusive);
*/