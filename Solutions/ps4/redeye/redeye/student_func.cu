//Udacity HW 4
//Radix Sorting

#include "reference_calc.cpp"
#include "utils.h"
#include <stdio.h>
#include <bitset>
#include <cmath>
#include <algorithm>

/* Red Eye Removal
===============

For this assignment we are implementing red eye removal.  This is
accomplished by first creating a score for every pixel that tells us how
likely it is to be a red eye pixel.  We have already done this for you - you
are receiving the scores and need to sort them in ascending order so that we
know which pixels to alter to remove the red eye.

Note: ascending order == smallest to largest

Each score is associated with a position, when you sort the scores, you must
also move the positions accordingly.

Implementing Parallel Radix Sort with CUDA
==========================================

The basic idea is to construct a histogram on each pass of how many of each
"digit" there are.   Then we scan this histogram so that we know where to put
the output of each digit.  For example, the first 1 must come after all the
0s so we have to know how many 0s there are to be able to start moving 1s
into the correct position.

1) Histogram of the number of occurrences of each digit
2) Exclusive Prefix Sum of Histogram
3) Determine relative offset of each digit
For example [0 0 1 1 0 0 1]
->  [0 1 0 1 2 3 2]
4) Combine the results of steps 2 & 3 to determine the final
output location for each element and move it there

LSB Radix sort is an out-of-place sort and you will need to ping-pong values
between the input and output buffers we have provided.  Make sure the final
sorted results end up in the output buffer!  Hint: You may need to do a copy
at the end.

*/

const int maxCudaThreads = 1024;
const int radixDigits = 1;
const int radixBins = pow(2, radixDigits);

// Taken from stackoverflow
int roundUpPower2(int n)
{
	n--;           // 1000 0011 --> 1000 0010
	n |= n >> 1;   // 1000 0010 | 0100 0001 = 1100 0011
	n |= n >> 2;   // 1100 0011 | 0011 0000 = 1111 0011
	n |= n >> 4;   // 1111 0011 | 0000 1111 = 1111 1111
	n |= n >> 8;   // ... (At this point all bits are 1, so further bitwise-or
	n |= n >> 16;  //      operations produce no effect.)
	n++;           // 1111 1111 --> 1 0000 0000
	return n;
}

/***********************************
          Print/Debug
***********************************/
__global__ void countZeroBits(unsigned int* count, unsigned int* d_inputVals, unsigned int bit)
{
	int threadId_global = threadIdx.x + blockDim.x * blockIdx.x;
	int isZero = (d_inputVals[threadId_global] & (1 << bit)) >> bit;
	atomicAdd(count, isZero);
}

void printHistogram(const unsigned int* d_hist, int numBins){
	unsigned int *h_bins;
	h_bins = new unsigned int[numBins];
	checkCudaErrors(cudaMemcpy(h_bins, d_hist, numBins * sizeof(unsigned int), cudaMemcpyDeviceToHost));
	for (int i = 0; i < numBins; i++){
		printf("%d %u\n", i, h_bins[i]);
	}
	delete[] h_bins;
}

void printInputs(unsigned int* const d_inputVals,
	unsigned int* const d_inputPos,
	const size_t numElems,
	const unsigned* const d_predicate,
	const unsigned* const d_scan)
{
	unsigned *h_inputVals, *h_inputPos, *h_predicate, *h_scan;
	h_inputVals = new unsigned[numElems];
	h_inputPos = new unsigned[numElems];
	h_predicate = new unsigned[numElems];
	h_scan = new unsigned[numElems];
	checkCudaErrors(cudaMemcpy(h_inputVals, d_inputVals, sizeof(int) * numElems, cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(h_inputPos, d_inputPos, sizeof(int) * numElems, cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(h_predicate, d_predicate, sizeof(int) * numElems, cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(h_scan, d_scan, sizeof(int) * numElems, cudaMemcpyDeviceToHost));
	int numZeros = 0;
	int numZeros_original = 0;
	int numZeros_myway = 0;
	int outofOrderElems = 0;
	int nonConsecIndices = 0;
	int max = 0;
	int min = UINT_MAX;
	for (int i = 0; i < numElems; i++){
		if (h_inputVals[i] % 2 == 0) numZeros++;
		if ((h_inputVals[i] & (1 << 0)) >> 0 == 0) numZeros_original++;
		if ((h_inputVals[i] >> (0 * 1)) % 2 == 0) numZeros_myway++;
		std::bitset<32> bits(h_inputVals[i]);
		if (h_inputVals[i] > max){
			max = h_inputVals[i];
		}
		if (h_inputVals[i] < min){
			min = h_inputVals[i];
		}
		if (i > 0){
			if (h_inputVals[i] < h_inputVals[i - 1]){
				outofOrderElems++;
			}
			if (h_inputPos[i] != h_inputPos[i - 1] + 1){
				nonConsecIndices++;
			}
		}

		printf("%u %-7u %u %-7u ", h_inputVals[i], h_inputPos[i], h_predicate[i], h_scan[i]);
		std::cout << bits << std::endl;
	}
	printf("min %d max %d outOfOrder %d nonConsec %d\n", min, max, outofOrderElems, nonConsecIndices);
	//printf("zeros: %d\noriginal: %d\nmy way: %d\nnumElems: %d\n", numZeros, numZeros_original, numZeros_myway, numElems);
	delete[] h_inputVals;
	delete[] h_inputPos;
	delete[] h_predicate;
	delete[] h_scan;
}


/***********************************
           Histogram
***********************************/
__global__ void atomicHistogramShmem(unsigned* const d_hist,
	unsigned* const d_bins,
	const unsigned* const d_in,
	const size_t numElems,
	const size_t numBins,
	const int position,
	const int numDigits)
{
	int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
	int threadId_block = threadIdx.x;

	if (threadId_global >= numElems){
		return;
	}

	//Initialize shared mem histogram and sync
	extern __shared__ unsigned int sh_bins[];
	if (threadId_block < numBins){
		sh_bins[threadId_block] = 0;
	}
	__syncthreads();

	//Calc bin
	unsigned int bin = (d_in[threadId_global] >> (position * numDigits)) % numBins;

	//Increment shared histogram and sync
	atomicAdd(&(sh_bins[bin]), 1);
	__syncthreads();

	//To global histogram, add shared histogram bin totals
	//using thread id as an indexer
	if (threadId_block < numBins){
		atomicAdd(&(d_hist[threadId_block]), sh_bins[threadId_block]);
	}
	d_bins[threadId_global] = bin;
}

__global__ void initializeArray(unsigned int* d_in, const unsigned int val)
{
	int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
	d_in[threadId_global] = val;
}

/***********************************
        Compact Helpers
***********************************/
__global__ void createPredicateArray(unsigned* const d_predicate,
	const unsigned const *d_bins, const unsigned bin, size_t numElems)
{
	int threadId_global = threadIdx.x + blockIdx.x * blockDim.x;
	if (threadId_global >= numElems){
		return;
	}
	d_predicate[threadId_global] = d_bins[threadId_global] == bin ? 1 : 0;
}

/***********************************
     Blelloch Exclusive Scan
***********************************/
__global__ void initializeDownsweeps(unsigned int* d_in, int i, int blocksize, size_t numElems){
	if (threadIdx.x * blocksize + i >= numElems) return;
	d_in[threadIdx.x * blocksize + i] = 0;
}

__global__ void initializeDownsweep(unsigned int* d_in, int i){
	d_in[i] = 0;
}

__global__ void addGridScan(unsigned* const d_scan, unsigned* const d_scan_block, size_t numElems){
	int index_global = threadIdx.x + blockDim.x * blockIdx.x;
	int index_shared = threadIdx.x;

	if (index_global >= numElems){
		return;
	}

	d_scan[index_global] += d_scan_block[blockIdx.x];
}

__global__ void exclusiveScan(unsigned* const d_scan, unsigned* const d_scan_block, const unsigned * const d_orig_list, const unsigned n, size_t numElems){
	int index_global = threadIdx.x + blockDim.x * blockIdx.x;
	//int threadId_global = threadIdx.x + blockDim.x * blockIdx.x;
	int index_shared = threadIdx.x;

	// Use shared mem to reduce global read/writes
	extern __shared__ unsigned sh_scan[];
	if (index_global >= numElems){
		sh_scan[index_shared] = 0;
	}
	else {
		sh_scan[index_shared] = d_scan[index_global];
	}
	__syncthreads();

	// n is the smallest power of 2 greater than or equal to blockDim.x
	for (int s = 1; s <= n / 2; s <<= 1){
		if (index_shared % (s + s) == ((blockDim.x - 1) % (s + s)) && index_shared - s >= 0){
			sh_scan[index_shared] = sh_scan[index_shared - s] + sh_scan[index_shared];
		}
		__syncthreads();
	}

	if (index_shared == blockDim.x - 1) sh_scan[index_shared] = 0;

	// n is the smallest power of 2 greater than or equal to blockDim.x
	unsigned downsweep;
	for (int s = n / 2; s > 0; s >>= 1){
		if (index_shared % (s * 2) == ((blockDim.x - 1) % (s * 2))){
			if (index_shared - s >= 0){
				downsweep = sh_scan[index_shared];
				sh_scan[index_shared] = sh_scan[index_shared - s] + sh_scan[index_shared];
				sh_scan[index_shared - s] = downsweep;
			}
		}
		__syncthreads();
	}

	if (index_shared == blockDim.x - 1 || index_global == numElems - 1){
		d_scan_block[blockIdx.x] = sh_scan[index_shared] + d_orig_list[index_global];
	}
	if (index_global < numElems){
		d_scan[index_global] = sh_scan[index_shared];
	}
}

/***********************************
         Radix Helpers
***********************************/
__global__ void writePositions(unsigned* const d_position, unsigned* const d_predicate, unsigned* const d_scan, unsigned offset, size_t numElems)
{
	int index_global = threadIdx.x + blockIdx.x * blockDim.x;
	if (index_global >= numElems){
		return;
	}

	if (d_predicate[index_global] == 1){
		d_position[index_global] = d_scan[index_global] + offset;
	}
}

__global__ void reorderList(const unsigned* const d_inputVals, const unsigned* const d_inputPos, const unsigned* const d_position,
	unsigned* const d_outputVals, unsigned* const d_outputPos, size_t numElems)
{
	int index_global = threadIdx.x + blockIdx.x * blockDim.x;
	if (index_global >= numElems){
		return;
	}

	d_outputVals[d_position[index_global]] = d_inputVals[index_global];
	d_outputPos[d_position[index_global]] = d_inputPos[index_global];
}


/*
1. Histogram for each digit (or combo of digits) for determining partitions
2. For each digit/position
a. For each possible digit value
i.   Create a predicate array of 0's and 1's that can be summed
to find the index for each digit we're sorting on
ii.  Exclusive scan to find index within partition
iii. Get paritition offset from exclusive scan of histogram
iv.  Write items to new locations in output array
v.   Copy output back into input and loop
*/
void your_sort(unsigned int* const d_inputVals,
	unsigned int* const d_inputPos,
	unsigned int* const d_outputVals,
	unsigned int* const d_outputPos,
	const size_t numElems)
{
	int totalDigits = 32;
	unsigned int *d_hist, *d_bins, *h_hist;
	checkCudaErrors(cudaMalloc(&d_hist, sizeof(unsigned int) * radixBins));
	checkCudaErrors(cudaMalloc(&d_bins, sizeof(unsigned int) * numElems));
	h_hist = new unsigned[radixBins];
	int threads_hist = std::min(maxCudaThreads, radixBins);
	int blocks_hist = (radixBins + threads_hist - 1) / threads_hist;
	int threads_elems = std::min(maxCudaThreads, (int)numElems);
	int blocks_elems = (numElems + threads_elems - 1) / threads_elems;

	unsigned int *d_predicate, *d_scan, *d_scan_block, *d_scan_block_placeholder, *d_positions;
	checkCudaErrors(cudaMalloc(&d_predicate, sizeof(unsigned int) * numElems));
	checkCudaErrors(cudaMalloc(&d_positions, sizeof(unsigned int) * numElems));
	checkCudaErrors(cudaMalloc(&d_scan, sizeof(unsigned int) * numElems));
	checkCudaErrors(cudaMalloc(&d_scan_block_placeholder, sizeof(unsigned int) * 1));
	int threads_localReduce, blocks_localReduce, n;

	for (int pos = 0; pos < totalDigits; pos += radixDigits){

		initializeArray << <blocks_hist, threads_hist >> >(d_hist, 0);
		atomicHistogramShmem << <blocks_elems, threads_elems, sizeof(unsigned int) * radixBins >> >(d_hist, d_bins, d_inputVals, numElems, radixBins, pos, radixDigits);

		// Copy histogram to host to pass the partitions later on
		checkCudaErrors(cudaMemcpy(h_hist, d_hist, sizeof(unsigned int) * radixBins, cudaMemcpyDeviceToHost));

		unsigned offset = 0;
		for (int bin = 0; bin < radixBins; bin++){
			// d_predicate can be written and overwritten
			createPredicateArray << <blocks_elems, threads_elems >> >(d_predicate, d_bins, bin, numElems);

			// d_scan can be written and overwritten
			checkCudaErrors(cudaMemcpy(d_scan, d_predicate, sizeof(unsigned int) * numElems, cudaMemcpyDeviceToDevice));

			// Scan at the local level
			threads_localReduce = std::min(maxCudaThreads, (int)numElems);
			blocks_localReduce = (numElems + threads_localReduce - 1) / threads_localReduce;
			checkCudaErrors(cudaMalloc(&d_scan_block, sizeof(unsigned int) * blocks_localReduce));
			n = roundUpPower2(threads_localReduce);
			exclusiveScan << <blocks_localReduce, threads_localReduce, sizeof(unsigned) * threads_localReduce >> >(d_scan, d_scan_block, d_predicate, n, numElems);

			// If scan doesn't fit in one block, scan at grid level and then add
			if (numElems > maxCudaThreads){
				n = roundUpPower2(blocks_localReduce);
				exclusiveScan << <1, blocks_localReduce, sizeof(unsigned) * blocks_localReduce >> >(d_scan_block, d_scan_block_placeholder, d_scan, n, blocks_localReduce);
				addGridScan << <blocks_localReduce, threads_localReduce >> >(d_scan, d_scan_block, numElems);
			}

			if (bin > 0){ // in lieu of exclusive scanning the histogram..
				for (int i = 0; i < bin; i++){
					offset += h_hist[i];
				}
			}
			writePositions << <blocks_elems, threads_elems >> >(d_positions, d_predicate, d_scan, offset, numElems);
			checkCudaErrors(cudaFree(d_scan_block));
		}

		reorderList << <blocks_elems, threads_elems >> >(d_inputVals, d_inputPos, d_positions, d_outputVals, d_outputPos, numElems);
		if (pos != totalDigits - 1){
			checkCudaErrors(cudaMemcpy(d_inputVals, d_outputVals, sizeof(unsigned) * numElems, cudaMemcpyDeviceToDevice));
			checkCudaErrors(cudaMemcpy(d_inputPos, d_outputPos, sizeof(unsigned) * numElems, cudaMemcpyDeviceToDevice));
		}
	}

	//printInputs(d_outputVals, d_outputPos, numElems, d_predicate, d_scan);
	//printHistogram(d_hist, radixBins);
	checkCudaErrors(cudaFree(d_bins));
	checkCudaErrors(cudaFree(d_scan));
	checkCudaErrors(cudaFree(d_positions));
	checkCudaErrors(cudaFree(d_scan_block_placeholder));
	checkCudaErrors(cudaFree(d_predicate));
	checkCudaErrors(cudaFree(d_hist));
	delete[] h_hist;
}
