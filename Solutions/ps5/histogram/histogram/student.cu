/* Udacity HW5
   Histogramming for Speed

   The goal of this assignment is compute a histogram
   as fast as possible.  We have simplified the problem as much as
   possible to allow you to focus solely on the histogramming algorithm.

   The input values that you need to histogram are already the exact
   bins that need to be updated.  This is unlike in HW3 where you needed
   to compute the range of the data and then do:
   bin = (val - valMin) / valRange to determine the bin.

   Here the bin is just:
   bin = val

   so the serial histogram calculation looks like:
   for (i = 0; i < numElems; ++i)
     histo[val[i]]++;

   That's it!  Your job is to make it run as fast as possible!

   The values are normally distributed - you may take
   advantage of this fact in your implementation.

*/


#include "utils.h"
const unsigned MAX_THREADS_BLOCK = 1024;
__global__
void serialHistogram(const unsigned int* const vals,
               unsigned int* const hist,
               unsigned int numElems)
{
	for (int i = 0; i < numElems; ++i){
		hist[vals[i]]++;
	}
}

__global__
void atomicHistogram(const unsigned int* const vals,
unsigned int* const hist,
unsigned int numElems)
{
	atomicAdd(&hist[vals[threadIdx.x + blockIdx.x * blockDim.x]], 1);
}

__global__
void atomicShmemHistogram(const unsigned int* const vals,
unsigned int* const hist,
unsigned int numElems)
{
	// Can skip this as long as # elems / # threads	has no remainder
	//if (threadIdx.x + blockIdx.x * blockDim.x >= numElems){
	//	return;
	//}
	
	extern __shared__ unsigned int sh_hist[];
	sh_hist[threadIdx.x] = 0; // # Threads must == # Bins
	__syncthreads();

	atomicAdd(&sh_hist[vals[threadIdx.x + blockIdx.x * blockDim.x]], 1);
	__syncthreads();

	atomicAdd(&hist[threadIdx.x], sh_hist[threadIdx.x]);
}

__global__
void atomicDoubleShmemHistogram(const unsigned int* const vals,
unsigned int* const hist,
unsigned int numElems)
{
	extern __shared__ unsigned int sh_hist[];
	sh_hist[threadIdx.x] = 0; // # Threads must == # Bins
	__syncthreads();

	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 2]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 2 + 1]], 1);
	//atomicAdd(&sh_hist[vals[threadIdx.x + blockIdx.x * blockDim.x + numElems / 2]], 1);
	__syncthreads();

	atomicAdd(&hist[threadIdx.x], sh_hist[threadIdx.x]);
}

__global__
void atomicQuadShmemHistogram(const unsigned int* const vals,
unsigned int* const hist,
unsigned int numElems)
{
	extern __shared__ unsigned int sh_hist[];
	sh_hist[threadIdx.x] = 0; // # Threads must == # Bins
	__syncthreads();

	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 4]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 4 + 1]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 4 + 2]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 4 + 3]], 1);
	__syncthreads();

	atomicAdd(&hist[threadIdx.x], sh_hist[threadIdx.x]);
}

__global__
void atomicSixteenShmemHistogram(const unsigned int* const vals,
unsigned int* const hist,
unsigned int numElems)
{
	extern __shared__ unsigned int sh_hist[];
	sh_hist[threadIdx.x] = 0; // # Threads must == # Bins
	__syncthreads();

	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 1]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 2]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 3]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 4]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 5]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 6]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 7]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 8]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 9]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 10]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 11]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 12]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 13]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 14]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 16 + 15]], 1);
	__syncthreads();

	atomicAdd(&hist[threadIdx.x], sh_hist[threadIdx.x]);
}

__global__
void atomicEightyShmemHistogram(const unsigned int* const vals,
unsigned int* const hist,
unsigned int numElems)
{
	extern __shared__ unsigned int sh_hist[];
	sh_hist[threadIdx.x] = 0; // # Threads must == # Bins
	__syncthreads();

	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 1]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 2]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 3]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 4]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 5]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 6]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 7]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 8]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 9]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 10]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 11]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 12]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 13]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 14]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 15]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 16]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 17]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 18]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 19]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 20]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 21]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 22]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 23]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 24]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 25]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 26]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 27]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 28]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 29]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 30]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 31]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 32]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 33]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 34]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 35]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 36]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 37]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 38]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 39]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 40]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 41]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 42]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 43]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 44]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 45]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 46]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 47]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 48]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 49]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 50]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 51]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 52]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 53]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 54]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 55]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 56]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 57]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 58]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 59]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 60]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 61]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 62]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 63]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 64]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 65]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 66]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 67]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 68]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 69]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 70]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 71]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 72]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 73]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 74]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 75]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 76]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 77]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 78]], 1);
	atomicAdd(&sh_hist[vals[(threadIdx.x + blockIdx.x * blockDim.x) * 80 + 79]], 1);
	__syncthreads();

	atomicAdd(&hist[threadIdx.x], sh_hist[threadIdx.x]);
}

void computeHistogram(const unsigned int* const d_vals, //INPUT
                      unsigned int* const d_hist,      //OUTPUT
                      const unsigned int numBins,
                      const unsigned int numElems)
{
	// Serial
	//serialHistogram<< <1, 1 >> >(d_vals, d_hist, numElems);
	
	// Atomic
	//int atomicThreads = MAX_THREADS_BLOCK;
	//int atomicBlocks = (numElems + atomicThreads - 1) / atomicThreads;
	//atomicHistogram << <atomicBlocks, atomicThreads >> >(d_vals, d_hist, numElems);

	// Atomic + Shared Memory
	//int atomicShmemThreads = numBins;
	//int atomicShmemBlocks = (numElems + atomicShmemThreads - 1) / atomicShmemThreads;
	//atomicShmemHistogram << <atomicShmemBlocks, atomicShmemThreads, sizeof(unsigned) * numBins >> >(d_vals, d_hist, numElems);

	// Atomic + Shared Memory + Multiple Read
	//int atomicDoubleShmemThreads = numBins;
	//int atomicDoubleShmemBlocks = (numElems + atomicDoubleShmemThreads - 1) / atomicDoubleShmemThreads / 2;
	//atomicDoubleShmemHistogram << <atomicDoubleShmemBlocks, atomicDoubleShmemThreads, sizeof(unsigned) * numBins >> >(d_vals, d_hist, numElems);

	//int atomicQuadShmemThreads = numBins;
	//int atomicQuadShmemBlocks = (numElems + atomicQuadShmemThreads - 1) / atomicQuadShmemThreads / 4;
	//atomicQuadShmemHistogram << <atomicQuadShmemBlocks, atomicQuadShmemThreads, sizeof(unsigned) * numBins >> >(d_vals, d_hist, numElems);

	int atomicSixteenShmemThreads = numBins;
	int atomicSixteenShmemBlocks = (numElems + atomicSixteenShmemThreads - 1) / atomicSixteenShmemThreads / 16;
	atomicSixteenShmemHistogram << <atomicSixteenShmemBlocks, atomicSixteenShmemThreads, sizeof(unsigned) * numBins >> >(d_vals, d_hist, numElems);

	//int atomicEightyShmemThreads = numBins;
	//int atomicEightyShmemBlocks = (numElems + atomicEightyShmemThreads - 1) / atomicEightyShmemThreads / 80;
	//atomicEightyShmemHistogram << <atomicEightyShmemBlocks, atomicEightyShmemThreads, sizeof(unsigned) * numBins >> >(d_vals, d_hist, numElems);

	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}
