# Udacity Parallel Programming
Class code for Udacity Parallel Programming course. Answers to problem sets are in Solutions directory. My solutions are setup to be run in VS 2013.

# Visual Studio 2013
* Step 1. Build and install OpenCV (tutorial here: http://docs.opencv.org/2.4/doc/tutorials/introduction/windows_install/windows_install.html). If you have trouble configuring with VS this tutorial is also helpful - http://opencv-srf.blogspot.com/2013/05/installing-configuring-opencv-with-vs.html. 
* Step 2. Create environmental variable OPENCV_DIR that points at C:/Program Files/opencv/build (or wherever your OpenCV is installed). For detailed overview of this process see: https://discussions.udacity.com/t/is-there-a-way-to-port-the-homework-files-into-visual-studio/83340/4.

# Building on OS X

These instructions are for OS X 10.9 "Mavericks".

* Step 1. Build and install OpenCV. The best way to do this is with
Homebrew. However, you must slightly alter the Homebrew OpenCV
installation; you must build it with libstdc++ (instead of the default
libc++) so that it will properly link against the nVidia CUDA dev kit. 
[This entry in the Udacity discussion forums](http://forums.udacity.com/questions/100132476/cuda-55-opencv-247-os-x-maverick-it-doesnt-work) describes exactly how to build a compatible OpenCV.

* Step 2. You can now create 10.9-compatible makefiles, which will allow you to
build and run your homework on your own machine:
```
mkdir build
cd build
cmake ..
make
```